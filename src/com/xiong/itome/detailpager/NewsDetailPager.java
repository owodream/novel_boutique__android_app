package com.xiong.itome.detailpager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import xyz.zinglizingli.hotbook.R;

import com.viewpagerindicator.TabPageIndicator;

import com.xiong.itome.activity.NewsItemDetailActivity;
import com.xiong.itome.bean.NewsTitleInf;

public class NewsDetailPager extends BasePager {
	private String[] titles = new String[] { "����", "���а�", "��׿", "�ֻ�", "����",
			"ƻ��", "WP", "Windows" };
	private String[] contents = new String[] { "����ҳ��", "���а�ҳ��", "��׿ҳ��", "�ֻ�ҳ��",
			"����ҳ��", "ƻ��ҳ��", "WPҳ��", "Windowsҳ��" };
	private int[] ids = new int[] { R.drawable.pic1, R.drawable.pic2,
			 R.drawable.pic4, R.drawable.pic5, R.drawable.pic6,
			R.drawable.pic7, R.drawable.pic8 };
	private String[] newsTitle = new String[] { "�й��ƶ�/��ͨ/������һ�ٿ��߲���飺����ϸ���ع�",
			"������Դ�������ģ����Ǹ���ô�죿", "�Ȳ����ǣ�΢���Ӧ���������г��ݶ��20.4%",
			"��ݮ������ڶ������Ƴ�Passport2�����ذ�׿ϵͳ", "����QQ�н���Ϣ��׼��ѧ����ƭ��Ԫ",
			"�ѶȽϴ󣬡�����̫����ITER�ƻ����Ƴ�", "����5000�伸�٣�����Ȧ�����Ĥ΢��ȥ����",
			"iPhone6s/6s Plus������ƻ����������ר��" };
	private TabPageIndicator indicator;
	private NewsTitleInf titleInfos;
	private ViewPager vpNews;
	private boolean hasFoot;
	private ListView lvNews;
	private NewsAdapter adapter;
	private ImageView progress;
	private Handler handerr = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			vpNews.setVisibility(View.VISIBLE);
			if (progress.getAnimation() != null) {
				progress.getAnimation().cancel();
			}
			progress.setVisibility(View.GONE);
		}

	};
	@SuppressLint("HandlerLeak")
	private Handler handerr2 = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (newsListAdapter != null) {
				ArrayList<String> list = new ArrayList<String>();
				list.add("΢����Win10�����ߣ�չʾӦ���̵�ɹ�����");
				list.add("��ʵ�����µġ��ڿ��ùݡ����ӷ������򵹱�");
				list.add("2015ȫ����ǿ�س�¯�����ն�ռ26��");
				list.add("���ң�Win10�콢�ֻ�Lumia950/XL���巢��������ʱ���ع�");
				list.add("Ϊɶ����һ������ԣ�������������");
				list.add("�ֻ����Գ��й���ѧ�����䣺�����������");
				list.add("����΢��ԼŮ���ѣ��������");
				list.add("��骣���������MX5 Pro����3000Ԫ�ĸ߶˻���");
				String[] add = new String[newsTitle.length + 8];
				int j = 0;
				for (int i = 0; i < newsTitle.length; i++) {
					add[i] = newsTitle[i];
				}
				for (int i = add.length - 1; i >= newsTitle.length; i--) {
					if (j < 8) {
						add[i] = list.get(j);

						j++;
					}

				}

				newsTitle = add;

				newsListAdapter.notifyDataSetChanged();
			}
		}

	};
	private NewsTitleAdapter titleAdapter;
	private NewsListAdapter newsListAdapter;
	

	public NewsDetailPager(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View initView() {
		View view = View.inflate(ctx, R.layout.news_detail_pager, null);
		indicator = (TabPageIndicator) view.findViewById(R.id.indicator);
		vpNews = (ViewPager) view.findViewById(R.id.vp_news_detail);
		progress = (ImageView) view.findViewById(R.id.progress);
		titleInfos = new NewsTitleInf();
		
		adapter = new NewsAdapter();

		vpNews.setAdapter(adapter);
		indicator.setViewPager(vpNews);
		

		return view;
	}

	/**
	 * �ӷ������˻�ȡ����
	 * 
	 * @param position
	 */
	private void getDataFromService(int position) {
		vpNews.setVisibility(View.INVISIBLE);
		progress.setVisibility(View.VISIBLE);
		startAnim(progress);
		Bitmap pic1 = BitmapFactory.decodeResource(ctx.getResources(),
				R.drawable.title1);
		Bitmap pic2 = BitmapFactory.decodeResource(ctx.getResources(),
				R.drawable.title2);
		Bitmap pic3 = BitmapFactory.decodeResource(ctx.getResources(),
				R.drawable.title3);
		titleInfos.pic = new ArrayList<Bitmap>();
		titleInfos.pic.add(pic1);
		titleInfos.pic.add(pic2);
		titleInfos.pic.add(pic3);
	View headview= View.inflate(ctx, R.layout.news_head, null);
		
ViewPager vpTitle=(ViewPager) headview.findViewById(R.id.vp_news_titil);
		titleAdapter = new NewsTitleAdapter();
		vpTitle.setAdapter(titleAdapter);
		newsListAdapter = new NewsListAdapter();
		lvNews.addHeaderView(headview);
		lvNews.setAdapter(newsListAdapter);
		handerr.sendEmptyMessageDelayed(0, 2000);

	}

	/**
	 * ��ʼ����������
	 * 
	 * @param ������
	 */
	private void startAnim(ImageView progress2) {
		RotateAnimation anim = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		anim.setDuration(2000);
		anim.setRepeatCount(60);
		progress2.startAnimation(anim);

	}

	class NewsAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return titles.length;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return titles[position];
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			View newsDetailview = View.inflate(ctx,
					R.layout.news_view_pager_item, null);

			lvNews = (ListView) newsDetailview.findViewById(R.id.lv_news);
			lvNews.setOnScrollListener(new OnScrollListener() {

				private TextView footview;

				@Override
				public void onScrollStateChanged(AbsListView view,
						int scrollState) {
					if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
						// System.out.println("view.getLastVisiblePosition():"+lvNews.getLastVisiblePosition());
						// System.out.println("view.getFirstVisiblePosition():"+lvNews.getFirstVisiblePosition());

					}

				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,
						int visibleItemCount, int totalItemCount) {
					System.out.println("view.getFirstVisiblePosition():"
							+ firstVisibleItem);
					System.out.println("view.getLatsiblePosition():"
							+ (firstVisibleItem + visibleItemCount));
					if (firstVisibleItem + visibleItemCount == newsTitle.length) {
						if(footview!=null){
						lvNews.removeFooterView(footview);
						}
						footview = new TextView(ctx);
						footview.setText("���ظ��ࡣ����");

						footview.setGravity(Gravity.CENTER);
						
						lvNews.addFooterView(footview);
						lvNews.setSelection(newsTitle.length);
						
					
						handerr2.sendEmptyMessageDelayed(0, 2000);
					}
				}
			});
			lvNews.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Intent intent=new Intent(ctx,NewsItemDetailActivity.class);
					ctx.startActivity(intent);
					
				}
			});
			getDataFromService(position);

			container.addView(newsDetailview);
			return newsDetailview;
		}

	}

	class NewsTitleAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return titleInfos.pic.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			ImageView view = new ImageView(ctx);
			BitmapDrawable drawable = new BitmapDrawable(
					titleInfos.pic.get(position));
			view.setBackgroundDrawable(drawable);

			container.addView(view);
			return view;
		}

	}

	class NewsListAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return newsTitle.length + 1;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return newsTitle[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;
			View view = null;
			if (convertView == null) {
				holder = new ViewHolder();
				view = View.inflate(ctx, R.layout.news_list_item, null);
				holder.ivPic = (ImageView) view.findViewById(R.id.iv_news_pic);
				holder.tvTitle = (TextView) view
						.findViewById(R.id.tv_news_title);
				holder.tvTime = (TextView) view.findViewById(R.id.tv_news_time);
				holder.tvWatch = (TextView) view.findViewById(R.id.tv_watch);
				holder.tvComment = (TextView) view
						.findViewById(R.id.tv_comment);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
				view = convertView;
			}
			Random ran = new Random();

			holder.ivPic.setBackgroundResource(ids[ran.nextInt(8)]);
			holder.tvTime.setText(DateFormat.getTimeFormat(ctx).format(
					new Date()));
			holder.tvTitle.setText(newsTitle[ran.nextInt(newsTitle.length)]);
			int watch = ran.nextInt(10000);
			holder.tvComment.setText(ran.nextInt(watch+1)+"");
			holder.tvWatch.setText(watch + "");
			return view;
		}

	}

	class ViewHolder {
		ImageView ivPic;
		TextView tvTitle;
		TextView tvTime;
		TextView tvWatch;
		TextView tvComment;

	}

}
