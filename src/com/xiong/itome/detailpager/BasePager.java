package com.xiong.itome.detailpager;

import android.content.Context;
import android.view.View;
/**
 * 详情页基类
 * @author 熊小洋
 *
 */
public abstract class BasePager {
	public Context ctx;
	public BasePager(Context ctx) {
		this.ctx=ctx;
	}
	/**
	 * 初始化详情页视图
	 * @return
	 */
	public abstract View initView();
	/**
	 * 初始化详情页数据
	 */
	public void initData(){}
	
}
