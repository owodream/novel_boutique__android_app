package com.xiong.itome.factory;

import android.content.Context;

import com.xiong.itome.detailpager.BasePager;
import com.xiong.itome.detailpager.DisDetailPager;
import com.xiong.itome.detailpager.MeDetailPager;
import com.xiong.itome.detailpager.NewsDetailPager;

public class DetailPagerFactory {
	private static DetailPagerFactory factory;
	private DetailPagerFactory(){}
	public static DetailPagerFactory getInstance(){
		if(factory==null){
			factory=new DetailPagerFactory();
		}
		return factory;
	}
	public BasePager createDetailPager(Context ctx,int position){
		BasePager pager = null;
		switch (position) {
		case 0:
			pager=new NewsDetailPager(ctx);
			break;
		case 1:
			pager=new DisDetailPager(ctx);
			break;
		case 2:
			pager=new MeDetailPager(ctx);
			break;

		
		}
		return pager;
		
	}
}
