package com.xiong.itome;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import xyz.zinglizingli.hotbook.R;

import com.xiong.itome.detailpager.BasePager;
import com.xiong.itome.factory.DetailPagerFactory;
import com.xiong.itome.view.NoScrollViewPager;

/**
 * ��ҳ��Activity
 * 
 * @author ��С��
 * 
 */
public class HomeActivity extends Activity {
	private NoScrollViewPager vpHome;
	private RadioButton rbNews;
	private RadioButton rbDis;
	private RadioButton rbMe;
	private HomeAdapter adapter;
	private RadioGroup rgBottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		initView();
		 // ʵ���� LayoutParams����Ҫ��
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams( FrameLayout.LayoutParams.FILL_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT);

        // ���ù����������λ��
        layoutParams.gravity = Gravity.BOTTOM ; // ����ʾ��Ϊ���½�

        // ʵ���������

            // ���� Activity �� addContentView ����
	}

	/**
	 * ��ʼ���ؼ�
	 */
	private void initView() {

		rgBottom = (RadioGroup) findViewById(R.id.rg_foot);
		vpHome = (NoScrollViewPager) findViewById(R.id.vp_content);
		rbNews = (RadioButton) findViewById(R.id.rb_news);
		rbDis = (RadioButton) findViewById(R.id.rb_dis);
		rbMe = (RadioButton) findViewById(R.id.rb_me);

		adapter = new HomeAdapter();
		vpHome.setAdapter(adapter);
		// ����RADIOButoon��ѡ�����
		rgBottom.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				int currentItem = 0;
				switch (checkedId) {
				case R.id.rb_news:
					currentItem = 0;
					break;
				case R.id.rb_dis:
					currentItem = 1;
					break;
				case R.id.rb_me:
					currentItem = 2;
					break;

				default:
					break;
				}
				vpHome.setCurrentItem(currentItem);// ��radioButton�ı�ʱ���ı���Ӧ��viewPagerҳ��

			}
		});

	}

	/**
	 * NoScrollViewPagerAdapter
	 */
	class HomeAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 3;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			BasePager pager = DetailPagerFactory.getInstance()
					.createDetailPager(HomeActivity.this, position);
			View view = pager.initView();
			container.addView(view);
			return view;
		}

	}
}
