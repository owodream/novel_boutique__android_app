package com.xiong.itome;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import xyz.zinglizingli.hotbook.R;
/**
 * ����ҳActivity
 * @author ��С��
 *
 */
public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ImageView iv=(ImageView) findViewById(R.id.iv_flash);//�������ҳlogͼƬ
		Animation anim=getAnima();//��ȡ����Ҳ�Ľ��䶯��
		iv.startAnimation(anim);//��ʼ����ҳ�Ķ���
		anim.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
				
			}
			/**
			 * ������������
			 */
			@Override
			public void onAnimationEnd(Animation animation) {
				startHomeActivity();//��ʼ��ת��ҳ��
				
			}
		});
		
	}
	/**
	 * ��ת��ҳ��
	 */
	protected void startHomeActivity() {
		Intent intent=new Intent(this,HomeActivity.class);
		startActivity(intent);
		finish();
		
	}
	/**
	 * ��ȡ���䶯��
	 * @return
	 */
	private Animation getAnima() {
		AlphaAnimation anim=new AlphaAnimation(0.8f, 1);
		anim.setDuration(2000);
		anim.setFillAfter(true);
		return anim;
	}


}
