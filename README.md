# 小说精品屋-安卓APP

#### 介绍

小说精品屋-安卓APP为[《小说精品屋》](https://www.oschina.net/p/fiction_house)项目的安卓版本源码，使用JAVA语言开发。包含精品小说专区和轻小说专区。包括小说分类、小说搜索、小说排行、完本小说、小说评分、小说在线阅读、小说书架、阅读记录、小说下载、小说弹幕、小说自动爬取、小说内容自动分享到微博、邮件自动推广、链接自动推送到百度搜索引擎等功能 。感兴趣的可以配合[《小说精品屋》](https://www.oschina.net/p/fiction_house)项目查看。

#### 软件截图

1.安卓APP小说首页

![mini1](./assets/Screenshot_2019-08-28-20-02-22-638_xyz.zinglizing.png)

2.安卓APP小说列表页

![mini1](./assets/Screenshot_2019-08-27-20-19-01-729_xyz.zinglizing.png)

3.安卓APP小说详情页

![mini1](./assets/Screenshot_2019-08-30-10-21-43-510_xyz.zinglizing.png)

4.安卓APP小说目录页

![mini1](./assets/Screenshot_2019-08-27-18-42-05-947_xyz.zinglizing2.png)

5.安卓APP小说内容页

![mini1](./assets/Screenshot_2019-08-27-18-43-15-385_xyz.zinglizing.png)

#### 安装包下载

[点击下载APK](https://gitee.com/xiongxyang/novel_boutique__android_app/raw/develop/HotBook.apk)